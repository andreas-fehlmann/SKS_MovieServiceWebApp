
package at.technikum.wien;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für actor complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="actor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="lastname" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="sex" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="birthdate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actor")
public class Actor {

    @XmlAttribute(name = "firstname")
    protected String firstname;
    @XmlAttribute(name = "lastname")
    protected String lastname;
    @XmlAttribute(name = "sex")
    protected String sex;
    @XmlAttribute(name = "birthdate")
    protected String birthdate;

    /**
     * Ruft den Wert der firstname-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Legt den Wert der firstname-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Ruft den Wert der lastname-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Legt den Wert der lastname-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLastname(String value) {
        this.lastname = value;
    }

    /**
     * Ruft den Wert der sex-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSex() {
        return sex;
    }

    /**
     * Legt den Wert der sex-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Ruft den Wert der birthdate-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBirthdate() {
        return birthdate;
    }

    /**
     * Legt den Wert der birthdate-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBirthdate(String value) {
        this.birthdate = value;
    }

}
