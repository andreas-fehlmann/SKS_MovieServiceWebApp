package clients;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * Created by andre on 04.01.2017.
 */
public class MovieAuthenticator {
    private MovieAuthenticator() {}

    public static void setAsDefault(String username, String password) {
        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password.toCharArray());
            }
        });
    }
}
