package clients;

import at.technikum.wien.MovieRootElement;
import at.technikum.wien.MovieWebService_Service;

import javax.xml.bind.JAXBContext;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

/**
 * Created by opq on 08.11.16.
 */
public class MovieServiceClients {
    public static void main(String[] argv) {
        MovieAuthenticator.setAsDefault("writer", "123");
        at.technikum.wien.MovieWebService service = new MovieWebService_Service().getMovieWebServicePort();

//    service.getMoviesContains("big").getMovies().getMovie().forEach(
//            movie -> System.out.println(movie.getTitle())
//    );

        if (argv[0] != null) {
            System.out.println(argv[0]);
            MovieRootElement list;
            try {
                list = JAXBContext
                        .newInstance(MovieRootElement.class)
                        .createUnmarshaller()
                        .unmarshal(new StreamSource(new File(argv[0])), MovieRootElement.class)
                        .getValue();

                list.getMovies().forEach(movie -> System.out.println(movie.getTitle()));
                service.persistMovies(list.getMovies());
            } catch (Exception e) {
                e.printStackTrace();
            }


//      service.persistMovies(list.getMovies().getMovie());
        }
    }
}
