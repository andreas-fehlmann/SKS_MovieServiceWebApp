-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('1', '1980.11.11', 'Martha', 'Stewart', 'FEMALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('2', '1981.11.11', 'Silvia', 'Muster', 'FEMALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('3', '1982.11.11', 'Henriette', 'Stiefel', 'FEMALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('4', '1983.11.11', 'Franz', 'Muck', 'MALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('5', '1984.11.11', 'Max', 'Molterer', 'MALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('6', '1985.11.11', 'Peter', 'Stewart', 'MALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('7', '1986.11.11', 'Anna', 'Stewart', 'FEMALE');
-- INSERT INTO db_movie.Actor (id, birthdate, firstname, lastname, sex) VALUES ('8', '1987.11.11', 'Gustav', 'Stewart', 'MALE');
--
--
-- INSERT INTO db_movie.Studio (id, countrycode, name, postcode) VALUES ('1', 'AT', 'Studio1', '1111');
-- INSERT INTO db_movie.Studio (id, countrycode, name, postcode) VALUES ('2', 'AT', 'Studio2', '2111');
-- INSERT INTO db_movie.Studio (id, countrycode, name, postcode) VALUES ('3', 'AT', 'Studio3', '3111');
-- INSERT INTO db_movie.Studio (id, countrycode, name, postcode) VALUES ('4', 'AT', 'Studio4', '4111');
--
--
-- INSERT INTO db_movie.Movie (id, description, genre, length, releaseyear, title, studio_id) VALUES ('1', 'bad movie', 'trash', '120', '2001', 'Horrorshow', '1');
-- INSERT INTO db_movie.Movie (id, description, genre, length, releaseyear, title, studio_id) VALUES ('2', 'worse movie', 'trash', '122', '2006', 'Horrorshow2', '2');
-- INSERT INTO db_movie.Movie (id, description, genre, length, releaseyear, title, studio_id) VALUES ('3', 'worst movie', 'trash', '5120', '2011', 'Horrorshow3', '3');
--
--
-- INSERT INTO db_movie.Actor_Movie (actors_id, movies_id) VALUES ('1', '1');
-- INSERT INTO db_movie.Actor_Movie (actors_id, movies_id) VALUES ('1', '2');
-- INSERT INTO db_movie.Actor_Movie (actors_id, movies_id) VALUES ('2', '3');
-- INSERT INTO db_movie.Actor_Movie (actors_id, movies_id) VALUES ('2', '1');
-- INSERT INTO db_movie.Actor_Movie (actors_id, movies_id) VALUES ('3', '2');
-- INSERT INTO db_movie.Actor_Movie (actors_id, movies_id) VALUES ('3', '3');

INSERT INTO db_movie.actor (`id`, `birthdate`, `firstname`, `lastname`, `sex`) VALUES ('1', '1960-12-03', 'Julianne', 'Moore', 'FEMALE');
INSERT INTO db_movie.actor (`id`, `birthdate`, `firstname`, `lastname`, `sex`) VALUES ('2', '1952-06-20', 'John', 'Goodman', 'MALE');
INSERT INTO db_movie.actor (`id`, `birthdate`, `firstname`, `lastname`, `sex`) VALUES ('3', '1949-12-04', 'Jeff', 'Bridges', 'MALE');
INSERT INTO db_movie.studio (`id`, `countrycode`, `name`, `postcode`) VALUES ('1', 'UK', 'Working Title Films', 'W1U 4AN');
INSERT INTO db_movie.studio (`id`, `countrycode`, `name`, `postcode`) VALUES ('2', 'US', 'DreamTitle Films', 'ASPLS1');