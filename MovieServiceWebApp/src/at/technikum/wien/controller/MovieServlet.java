package at.technikum.wien.controller;

import at.technikum.wien.service.MovieService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/movies")
public class MovieServlet extends HttpServlet {

    @Inject
    private MovieService movieService;

    public MovieServlet() {
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        movieService.getAllMovies().forEach(movie -> out.println("<h3>" + movie.getTitle() + "</h3>" + "<p>" + movie.getDescription() + "</p>"));
        out.println("");
        out.println("</body></html>");
    }
}
