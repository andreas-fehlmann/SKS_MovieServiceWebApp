package at.technikum.wien.controller;

import at.technikum.wien.service.ActorService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/actors")
public class ActorServlet extends HttpServlet {

    @Inject
    private ActorService actorService;

    public ActorServlet(){

    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        actorService.getAllActors().forEach(actor -> out.println("<h3>" + actor.getFirstname() + " " + actor.getLastname() + "</h3><p>" + actor.getSex() + ", " + actor.getBirthdate() + "</p>"));
        out.println("");
        out.println("</body></html>");
    }
}
