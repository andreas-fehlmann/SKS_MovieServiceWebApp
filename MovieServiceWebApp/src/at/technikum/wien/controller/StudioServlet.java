package at.technikum.wien.controller;

import at.technikum.wien.service.StudioService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/studios")
public class StudioServlet extends HttpServlet {

    @Inject
    StudioService studioService;

    public StudioServlet() {

    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        studioService.getAllStudios().forEach(studio -> out.println("<h3>" + studio.getName() + "</h3>"
                + "<p>" + studio.getCountrycode() + "</p>"
                + "<p>" + studio.getPostcode() + "</p>"));
        out.println("");
        out.println("</body></html>");

    }
}
