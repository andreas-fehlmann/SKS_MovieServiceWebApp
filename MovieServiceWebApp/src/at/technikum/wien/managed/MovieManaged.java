package at.technikum.wien.managed;

import at.technikum.wien.entity.Movie;
import at.technikum.wien.service.MovieService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@RequestScoped
@Named("reqMovie")
public class MovieManaged {
    @Inject
    MovieService movieService;

    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

}
