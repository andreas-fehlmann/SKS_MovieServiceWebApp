package at.technikum.wien.managed;

import at.technikum.wien.entity.Studio;
import at.technikum.wien.service.StudioService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@RequestScoped
@Named("reqStudio")
public class StudioManaged {

    @Inject
    private StudioService studioService;

    public List<Studio> getAllStudios(){
        return studioService.getAllStudios();
    }
}
