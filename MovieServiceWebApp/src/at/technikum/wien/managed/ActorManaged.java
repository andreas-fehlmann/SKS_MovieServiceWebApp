package at.technikum.wien.managed;

import at.technikum.wien.entity.Actor;
import at.technikum.wien.service.ActorService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


@RequestScoped
@Named("reqActor")
public class ActorManaged {

    @Inject
    private ActorService actorService;

    public List<Actor> getAllActors(){
        return actorService.getAllActors();
    }
}
