package at.technikum.wien.xml.helper;

import at.technikum.wien.entity.Movie;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement(name = "movies")
//@XmlAccessorType(XmlAccessType.FIELD)
public class MovieRootElement {
    @XmlElement(name = "movie")
    private List<Movie> movies;

    public MovieRootElement() {
    }

    public MovieRootElement(List<Movie> movies) {
        this.movies = movies;
    }


    public List<Movie> getMovies() {
        return this.movies;
    }

}