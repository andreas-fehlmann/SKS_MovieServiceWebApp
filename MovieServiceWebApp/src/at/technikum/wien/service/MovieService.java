package at.technikum.wien.service;

import at.technikum.wien.entity.Actor;
import at.technikum.wien.entity.Movie;
import at.technikum.wien.entity.Studio;
import org.jboss.ejb3.annotation.SecurityDomain;



import javax.ejb.EJBException;

import javax.annotation.security.RolesAllowed;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;


@Stateless
@SecurityDomain("MovieSD")
@RolesAllowed("MSWrite")
public class MovieService {

    @PersistenceContext
    private EntityManager entityManager;


    public List<Movie> getAllMovies(){
        return entityManager.createNamedQuery("Movie.getAll", Movie.class)
                .getResultList();
    }

    @TransactionAttribute(value= TransactionAttributeType.REQUIRED)
    public void persistTransactionally(List<Movie> movies) throws Exception {
        for (Movie movie: movies) {
            List<Actor> actors = getPersistedActors(movie.getActors());
            Studio studio = getPersistedStudio(movie.getStudio());
            if (movieExists(movie))
                throw new EJBException(String.format("Movie %s couldn't be added, reason: already exists!", movie.getTitle()));

            movie.setActors(actors);
            movie.setStudio(studio);
            entityManager.persist(movie);
        }
    }

    private List<Actor> getPersistedActors(List<Actor> actors) {
        List<Actor> persistedActores = new ArrayList<>();
        for (Actor actor: actors) {
            // Throws an exception if there is no or more than one result on the db
            Actor persistedActor = entityManager.createNamedQuery("Actor.getActorCount", Actor.class)
                    .setParameter("birthdate", actor.getBirthdate())
                    .setParameter("firstname", actor.getFirstname())
                    .setParameter("lastname", actor.getLastname())
                    .setParameter("sex", actor.getSex()).getSingleResult();
            persistedActores.add(persistedActor);
        }
        return persistedActores;
    }

    private Studio getPersistedStudio(Studio studio) {
        return entityManager.createNamedQuery("Studio.getStudioCount", Studio.class)
                .setParameter("name", studio.getName())
                .setParameter("countrycode", studio.getCountrycode())
                .setParameter("postcode", studio.getPostcode()).getSingleResult();
    }

    private boolean movieExists(Movie movie) {
        return 0 != entityManager.createNamedQuery("Movie.getMovieCount", Movie.class)
                .setParameter("title", movie.getTitle())
                .setParameter("description", movie.getDescription())
                .setParameter("genre", movie.getGenre())
                .setParameter("releaseyear", movie.getReleaseYear())
                .setParameter("length", movie.getLength()).getResultList().size();
    }


    public List<Movie> contains(String name) {
        return entityManager.createNamedQuery("Movie.getByTitle", Movie.class)
                .setParameter("name", name.toLowerCase()).getResultList();
    }
}
