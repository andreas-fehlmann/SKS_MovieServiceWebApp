package at.technikum.wien.service;

import at.technikum.wien.entity.Actor;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import org.jboss.ejb3.annotation.SecurityDomain;


@Stateless
@SecurityDomain("MovieSD")
@RolesAllowed("MSWrite")
public class ActorService {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Actor> getAllActors() {
        return entityManager.createNamedQuery("Actor.getAll", Actor.class).getResultList();
    }

    public void persist(Actor saveActor) {
        entityManager.persist(saveActor);
    }

    public List<Actor> findByName(String searchName) {
        return entityManager.createNamedQuery("Actor.getByName", Actor.class)
                .setParameter("name", searchName).getResultList();
    }

    public void remove(Actor actor) {
        System.out.println(actor.toString());
        Actor actor1 = entityManager.contains(actor) ? actor : entityManager.merge(actor);
        entityManager.remove(actor1);
    }
}
