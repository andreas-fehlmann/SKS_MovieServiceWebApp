package at.technikum.wien.service;

import at.technikum.wien.entity.Studio;
import org.jboss.ejb3.annotation.SecurityDomain;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@SecurityDomain("MovieSD")
@RolesAllowed("MSWrite")
public class StudioService {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Studio> getAllStudios() {
        return entityManager.createNamedQuery("Studio.getAll", Studio.class).getResultList();
    }

    public void persist(Studio saveStudio) {
        entityManager.persist(saveStudio);
    }

    public List<Studio> findByName(String searchName) {
        return entityManager.createNamedQuery("Studio.getByName", Studio.class)
                .setParameter("name", searchName).getResultList();
    }

    public void remove(Studio studio) {
        entityManager.remove(studio);
    }
}
