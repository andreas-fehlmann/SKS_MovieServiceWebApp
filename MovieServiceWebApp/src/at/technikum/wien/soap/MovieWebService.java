package at.technikum.wien.soap;

import at.technikum.wien.entity.Movie;
import at.technikum.wien.xml.helper.MovieRootElement;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;


@WebService
interface MovieWebService {
    MovieRootElement getAllMovies();
    MovieRootElement getMoviesContains(String name);

//    String persistMoviesFromSoap(@WebParam(name = "movie") String soapInput);
    String persistMovies(@WebParam(name = "movie") List<Movie> movies);
}
