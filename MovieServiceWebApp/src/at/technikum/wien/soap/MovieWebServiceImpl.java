package at.technikum.wien.soap;

import at.technikum.wien.entity.Movie;
import at.technikum.wien.service.MovieService;
import at.technikum.wien.xml.helper.MovieRootElement;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.util.List;

@WebService(endpointInterface = "at.technikum.wien.soap.MovieWebService",
        serviceName = "MovieWebService",
        portName = "MovieWebServicePort")
public class MovieWebServiceImpl implements MovieWebService {
    @Inject
    private MovieService movieService;

    @Override
    @WebMethod
    public MovieRootElement getAllMovies() {
        return new MovieRootElement(movieService.getAllMovies());
    }

    @Override
    public MovieRootElement getMoviesContains(String name) {
        return new MovieRootElement(movieService.contains(name));
    }

    @Override
    public String persistMovies(List<Movie> movies) {
        try{
            movieService.persistTransactionally(movies);
            return "200 OK";
        }catch (Exception e) {
            return "500 Internal Server Error " + e.getMessage();
        }
    }
}
