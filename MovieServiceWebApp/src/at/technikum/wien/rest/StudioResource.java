package at.technikum.wien.rest;

import at.technikum.wien.entity.Actor;
import at.technikum.wien.entity.Studio;
import at.technikum.wien.service.StudioService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/studio")
@Transactional
public class StudioResource {
    @Inject
    private StudioService studioService;
    @PersistenceContext
    private EntityManager em;

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createStudio(Studio studio){
        studioService.persist(studio);
        return Response.status(200).build();
    }

    @GET
    @Path("/{studioId}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getStudioAsString(@PathParam("studioId") int studioId) {
        Studio studio = em.find(Studio.class, studioId);
        return (studio != null ? studio.toString() : null);
    }

    @GET
    @Path("/{studioId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Studio getStudioAsJSONXML(@PathParam("studioId") int studioId) {
        return em.find(Studio.class, studioId);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Studio> getAllNewsXML() {
        return studioService.getAllStudios();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public List<Studio> getAllNewsJSON() {

        return studioService.getAllStudios();
    }

    @PUT
    @Path("/{studioId}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void updateStudio(@PathParam("studioId") int studioId, Studio studio) {
        Studio studioOld = em.find(Studio.class, studioId);
        if (studioOld != null) {
            studioOld.setName(studio.getName());
            studioOld.setCountrycode(studio.getCountrycode());
            studioOld.setPostcode(studio.getPostcode());
        }
        else {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @DELETE
    @Path("/{studioId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteStudio(@PathParam("studioId") int studioId) {
        Studio toDelete = em.find(Studio.class, studioId);

        if (toDelete != null) {
            studioService.remove(toDelete);
            return Response.status(200).build();
        } else {
            return Response.status(404).entity(toDelete).build();
        }
    }

}
