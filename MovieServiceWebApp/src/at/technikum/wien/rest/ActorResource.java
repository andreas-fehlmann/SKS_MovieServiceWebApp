package at.technikum.wien.rest;

import at.technikum.wien.entity.Actor;
import at.technikum.wien.service.ActorService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import  javax.transaction.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/actor")
@Transactional
public class ActorResource extends Application{
    @Inject
    private ActorService actorService;
    @PersistenceContext
    private EntityManager em;

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createActor(Actor actor){
        actorService.persist(actor);
        return Response.status(200).build();
    }

    @GET
    @Path("/{actorId}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getActorAsString(@PathParam("actorId") int actorId) {
        Actor actor = em.find(Actor.class, actorId);
        return (actor != null ? actor.toString() : null);
    }

    @GET
    @Path("/{actorId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Actor getActorAsXML(@PathParam("actorId") int actorId) {
        return em.find(Actor.class, actorId);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Actor> getAllNews() {
        return actorService.getAllActors();
    }

    @PUT
    @Path("/{actorId}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateActor(@PathParam("actorId") int actorId, Actor actor) {
        Actor actorOld = em.find(Actor.class, actorId);
        if (actorOld != null) {
            actorOld.setFirstname(actor.getFirstname());
            actorOld.setLastname(actor.getLastname());
            actorOld.setSex(actor.getSex());
            actorOld.setBirthdate(actor.getBirthdate());
//            em.merge(actorOld);
            return Response.ok().status(200).build();
        }
        else {
            System.out.println("Couldn't find entity");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @DELETE
    @Path("/{actorId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteActor(@PathParam("actorId") int actorId) {
        Actor toDelete = em.find(Actor.class, actorId);
        if (toDelete != null) {
            actorService.remove(toDelete);
            return Response.status(200).build();
        } else {
            return Response.status(404).entity(toDelete).build();
        }
    }
}