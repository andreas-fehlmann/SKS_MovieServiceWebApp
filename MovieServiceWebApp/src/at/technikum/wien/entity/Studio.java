package at.technikum.wien.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement
//@XmlType(propOrder = {"id", "name", "countrycode", "postcode", "id"})
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="studio")
@NamedQueries({
        @NamedQuery(name="Studio.getAll", query = "select s from Studio s"),
        @NamedQuery(name="Studio.getByName",
                query = "select s from Studio s " +
                        "where s.name like concat('%',:name,'%')"),
        @NamedQuery(name="Studio.getStudioCount",
                query = "select s from Studio s " +
                        "where s.name = :name " +
                        "and s.countrycode = :countrycode " +
                        "and s.postcode = :postcode")
})
public class Studio {

//    @XmlAttribute
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @XmlTransient
    private int id;

    @XmlAttribute(name="name")
    private String name;

    @XmlAttribute(name="countrycode")
    private String countrycode;

    @XmlAttribute(name="postcode")
    private String postcode;

    @XmlTransient
    @OneToMany (mappedBy = "studio", fetch = FetchType.EAGER)
    private List<Movie> movies;

    public String getPostcode() {
        return postcode;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
}