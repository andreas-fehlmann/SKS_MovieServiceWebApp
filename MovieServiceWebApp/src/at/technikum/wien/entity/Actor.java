package at.technikum.wien.entity;

import at.technikum.wien.xml.helper.DateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

@XmlRootElement
//@XmlType(propOrder = {"id", "firstname", "lastname", "sex", "birthdate", "id"})
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="actor")
@NamedQueries({
        @NamedQuery(name="Actor.getAll", query = "select a from Actor a"),
        @NamedQuery(name="Actor.getByName",
                query = "select a from Actor a " +
                        "where a.firstname like concat('%', :name, '%') " +
                        "or a.lastname like concat('%', :name, '%')"),
        @NamedQuery(name="Actor.getActorCount",
                query = "select a from Actor a " +
                        "where a.lastname like :lastname " +
                        "and a.firstname like :firstname " +
                        "and a.birthdate = :birthdate " +
                        "and a.sex like :sex")
})
public class Actor {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    @XmlTransient
    @XmlAttribute
    private int id;
    @XmlAttribute(name="firstname")
    private String firstname;
    @XmlAttribute(name="lastname")
    private String lastname;
    @XmlAttribute(name="sex")
    private String sex;
    @XmlJavaTypeAdapter(value = DateAdapter.class, type = java.util.Date.class)
    @XmlAttribute(name="birthdate")
    private Date birthdate;

    @XmlTransient
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Movie> movies;


    public String getFirstname() {
        return firstname;
    }



    public String getLastname() {
        return lastname;
    }

    public String getSex() {
        return sex;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}