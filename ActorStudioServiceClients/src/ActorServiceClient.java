import generated.Actor;


import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 * Created by Andi on 08.12.2016.
 */
public class ActorServiceClient {
    public static void main(String[] args) throws Exception {

        WebTarget target = ClientBuilder
                .newClient()
                .register(new RequestFilter("writer", "123"))
                .target("http://localhost:8080/MovieServiceWebApp_war_exploded/resources/actor");


        Actor actor = new Actor();
        actor.setFirstname(args[0]);
        actor.setLastname(args[1]);
        actor.setSex(args[2]);
        actor.setBirthdate(args[3]);

        System.out.println(args[0] + " " + args[1] + " " + args[2] + " " + args[3]);

        Response response = target
                .request(
                        MediaType.APPLICATION_JSON)
                .post(Entity.json(actor));
        System.out.println(response.getStatus());
    }
}
