
package generated;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import java.util.List;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 */
@WebService(name = "MovieWebService", targetNamespace = "http://soap.wien.technikum.at/")
@XmlSeeAlso({
        ObjectFactory.class
})
public interface MovieWebService {


    /**
     * @param arg0
     * @return returns at.technikum.wien.MovieRootElement
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getMoviesContains", targetNamespace = "http://soap.wien.technikum.at/", className = "at.technikum.wien.GetMoviesContains")
    @ResponseWrapper(localName = "getMoviesContainsResponse", targetNamespace = "http://soap.wien.technikum.at/", className = "at.technikum.wien.GetMoviesContainsResponse")
    MovieRootElement getMoviesContains(
            @WebParam(name = "arg0", targetNamespace = "")
                    String arg0);

    /**
     * @param movie
     * @return returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "persistMovies", targetNamespace = "http://soap.wien.technikum.at/", className = "at.technikum.wien.PersistMovies")
    @ResponseWrapper(localName = "persistMoviesResponse", targetNamespace = "http://soap.wien.technikum.at/", className = "at.technikum.wien.PersistMoviesResponse")
    String persistMovies(
            @WebParam(name = "movie", targetNamespace = "")
                    List<Movie> movie);

    /**
     * @return returns at.technikum.wien.MovieRootElement
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getAllMovies", targetNamespace = "http://soap.wien.technikum.at/", className = "at.technikum.wien.GetAllMovies")
    @ResponseWrapper(localName = "getAllMoviesResponse", targetNamespace = "http://soap.wien.technikum.at/", className = "at.technikum.wien.GetAllMoviesResponse")
    MovieRootElement getAllMovies();

}
