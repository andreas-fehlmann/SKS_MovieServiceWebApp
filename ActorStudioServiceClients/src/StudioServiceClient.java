import generated.Studio;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Andi on 08.12.2016.
 */
public class StudioServiceClient {
    public static void main(String[] args) throws Exception {
        WebTarget target = ClientBuilder
                .newClient()
                .register(new RequestFilter("writer", "123"))
                .target("http://localhost:8080/MovieServiceWebApp_war_exploded/resources/studio");


        Studio studio = new Studio();
        studio.setName(args[0]);
        studio.setCountrycode(args[1]);
        studio.setPostcode(args[2]);

        System.out.println(args[0] + " " + args[1] + " " + args[2]);

        Response response = target
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(studio));
        System.out.println(response.getStatus());
    }
}
