import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormGroup, FormBuilder,  Validators } from '@angular/forms';

import { AppComponent } from './app.component';
import { StudiosComponent } from './studios/studios.component';
import { ActorsComponent } from './actors/actors.component';
import { DalService } from './dal.service';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    StudiosComponent,
    ActorsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [
    FormBuilder,
    DalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
