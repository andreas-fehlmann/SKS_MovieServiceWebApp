import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder,  Validators } from '@angular/forms';
import { DalService } from '../dal.service';

@Component({
  selector: 'app-studios',
  templateUrl: './studios.component.html',
  styleUrls: ['./studios.component.css']
})
export class StudiosComponent implements OnInit {
  private studios: any;
  private details: string;
  public myForm: FormGroup;

  constructor( public dal : DalService,
               private fb: FormBuilder ) {
    this.getAllStudios();
    this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      postcode: ['', [Validators.required]],
      countrycode: ['',[Validators.required]],
      id: ['']
    });
  }

  ngOnInit(): void {
    this.getAllStudios();
  }

  ngOnDestroy(): void {

  }

  getAllStudios(): void {
    this.dal.callSmartApi('getAllStudios').subscribe(
      v => {
        this.studios = v.json();
      },
      e => {
        console.log(e);
      },
      () => {}
    );
  }

  onClick(s): void {
    this.details = '<h2>Details:</h2>';
    for (var property in s) {
        if (s.hasOwnProperty(property)) {
            this.details += property + ': ' + s[property] + '<br>';
        }
    }
    this.myForm.patchValue({name: s.name});
    this.myForm.patchValue({postcode: s.postcode});
    this.myForm.patchValue({countrycode: s.countrycode});
    this.myForm.patchValue({id: s.id});
  }

  deleteStudio(id): void {
    this.dal.callSmartApi('deleteStudio', id).subscribe(
      v=>{
        console.log(v);
      },
      e=>{
        console.log(e);
      },
      ()=>{
        this.getAllStudios();
      }
    );
  }
  savePUT(obj): void{
    console.log(obj);
    this.dal.callSmartApi('updateStudio', obj.id ,obj).subscribe(
      v=>{
        console.log(v);
      },
      e=>{
        console.log(e);
      },
      ()=>{
        this.getAllStudios();
      }
    );
  }

  savePOST(obj): void{
    console.log(obj);
    this.dal.callSmartApi('saveStudio', '', obj).subscribe(
      v=>{
        console.log(v);
      },
      e=>{
        console.log(e);
      },
      ()=>{
        this.getAllStudios();
      }
    );
  }

}
