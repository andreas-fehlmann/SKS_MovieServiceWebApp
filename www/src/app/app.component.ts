import { Component, OnInit, OnDestroy } from '@angular/core';
import { StudiosComponent } from './studios/studios.component';
import { ActorsComponent } from './actors/actors.component';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private title = 'SKS Frontend';



  constructor( ) {}

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }
}
