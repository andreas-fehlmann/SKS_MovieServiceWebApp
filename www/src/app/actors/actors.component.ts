import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder,  Validators } from '@angular/forms';
import { DalService } from '../dal.service';

@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.css']
})
export class ActorsComponent implements OnInit {

  private actors: any;
  private details: string;
  public myForm: FormGroup;

  constructor( public dal : DalService,
               private fb: FormBuilder ) {
    this.getAllActors();
    this.myForm = this.fb.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      sex: ['',[Validators.required]],
      birthdate: ['',[Validators.required]],
      id: ['']
    });
  }

  ngOnInit(): void {
    this.getAllActors();
  }

  ngOnDestroy(): void {

  }

  getAllActors(): void {
    this.dal.callSmartApi('getAllActors').subscribe(
      v => {
        this.actors = v.json();
      },
      e => {},
      () => {}
    );
  }

  onClick(s): void {
    this.details = '<h2>Details:</h2>';
    for (var property in s) {
        if (s.hasOwnProperty(property)) {
            this.details += property + ': ' + s[property] + '<br>';
        }
    }
    this.myForm.patchValue({firstname: s.firstname});
    this.myForm.patchValue({lastname: s.lastname});
    this.myForm.patchValue({sex: s.sex});
    this.myForm.patchValue({birthdate: s.birthdate});
    this.myForm.patchValue({id: s.id});
  }

  deleteActor(id): void {
    this.dal.callSmartApi('deleteActor', id).subscribe(
      v=>{
        console.log(v);
      },
      e=>{
        console.log(e);
      },
      ()=>{
        this.getAllActors();
      }
    );
  }

  savePUT(obj): void{
    console.log(obj);
    this.dal.callSmartApi('updateActor', obj.id ,obj).subscribe(
      v=>{
        console.log(v);
      },
      e=>{
        console.log(e);
      },
      ()=>{
        this.getAllActors();
      }
    );
  }

  savePOST(obj): void{
    console.log(obj);
    this.dal.callSmartApi('saveActor', '', obj).subscribe(
      v=>{
        console.log(v);
      },
      e=>{
        console.log(e);
      },
      ()=>{
        this.getAllActors();
      }
    );
  }

}
