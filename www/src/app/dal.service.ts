import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Headers } from '@angular/http';

@Injectable()
export class DalService {

  private api = {
          getAllActors: {path: '/resources/actor/', method: 'GET'},
          deleteActor:{path: '/resources/actor', method: 'DELETE'},
          updateActor:{path: '/resources/actor', method: 'PUT'},
          saveActor:{path: '/resources/actor', method: 'POST'},
          getAllStudios: { path: '/resources/studio/', method: 'GET' },
          deleteStudio:{path: '/resources/studio', method: 'DELETE'},
          updateStudio:{path: '/resources/studio', method: 'PUT'},
          saveStudio:{path: '/resources/studio', method: 'POST'},
        };

  constructor( public http: Http) {}

  public callSmartApi(apiCall: string, args?: string, body?: any){
    let bodyArray = {};
    if(body !== undefined){
      for (var property in body) {
          if (body.hasOwnProperty(property)) {
              bodyArray[property] = body[property];
          }
      }
    }
    let b = JSON.stringify(bodyArray);
    let path = this.api[apiCall].path;
    let method = this.api[apiCall].method;
    let addr = "http://localhost:8080/MovieServiceWebApp_war_exploded" + path;
    if(args !== undefined && args !== null && args !== '') {
      addr += '/' + args;
    }
    return this._callApi(method, addr, b);
  }

  // CRUD CALLS
  private _callApi( method, url, body? ): Observable<Response> {
    // For non-protected routes, just use Http
	let username : string = 'writer';
	let password : string = '123';
      if (method === 'GET') {  // read
        let contentHeaders = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');
		contentHeaders.append("Authorization", "Basic " + btoa(username + ":" + password));
		//contentHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.get(url, { headers: contentHeaders });
      }else if (method === 'PUT') {// update
        let contentHeaders = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');
		contentHeaders.append("Authorization", "Basic " + btoa(username + ":" + password));
		//contentHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.put(url, body, { headers: contentHeaders });
      }else if (method === 'POST') { // create
        let contentHeaders = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');
		contentHeaders.append("Authorization", "Basic " + btoa(username + ":" + password));
		//contentHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.post(url, body, { headers: contentHeaders });
      }else if (method === 'DELETE') { // delete
        let contentHeaders = new Headers();
        contentHeaders.append('Accept', 'text/plain');
		contentHeaders.append("Authorization", "Basic " + btoa(username + ":" + password));
		//contentHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.delete(url, { headers: contentHeaders });
      }
  }

}
